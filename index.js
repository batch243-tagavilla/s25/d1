// console.log(`Hello JSON`);

// [Section ] JSON Objects
    /* 
        - JSON stands for JavaScript Object Notation
        - is also used in other programming languages hence the name JSON
        - core JavaScript has a built in JSON object that contains methods for parsing JSON objects an convertiong strings into JavaScript objects
        - is used for serializing different data types.
        Syntax:
            {
                "propertyA" : "valueA",
                "propertyB" : "valueB"
            }
    */
   
    // JSON Object
    const jsonObject = {
        "city" : "Quezon City",
        "province" : "Metro Manila",
        "country" : "Philippines"
    }

    // JSON Arrays
    [
        {
            "city" : "Quezon City", 
            "province" : "Metro Manila", 
            "country" : "Philippines",
            "city" : "Manila City", 
            "province" : "Metro Manila", 
            "country" : "Philippines"
        }
    ]

// [Section] JSON Methods
    // 

    let batchesArr = 
    [
        {batchName: "Batch X"},
        {batchName: "Batch Y"}
    ];

    console.log("This is the original array: ");
    console.log(batchesArr);

    // The stringify method is used to convert JavaScript objects into a string
    console.log("Result from stringify method: ");
    let stringBatchesArr = JSON.stringify(batchesArr);
    console.log(stringBatchesArr);
    console.log(typeof(stringBatchesArr));

// [Section] Using stringify method with variables
    /* 
        - when information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
    */

    // User details
    /* 
    let firstName = prompt(`What is your first name?`);
    let lastName = prompt(`What is your last name?`);
    let age = prompt(`How young are you?`);
    let address = {
        city: prompt(`Which city do you live in?`),
        country: prompt(`Which country does your city address belong to?`)
    }

    let otherData = JSON.stringify(
        {
            fName: firstName,
            lName: lastName,
            age,
            address
        }
    )

    console.log(otherData);
    */

// [Section] Converting stringified JSON into JavaScript Objects
    /* 
        - objects are common data types used in application because of the value of the complex data structures that can be created out of them
        - information is commonly sent to applications in stringified JSON and then converted back into objects
        - this happens both for sending information to a back end application and sending information back to a frontend application
    */

   const parsedBatchesJSON = JSON.parse(stringBatchesArr);
   console.log("Result from JSON parse method: ");
   console.log(parsedBatchesJSON);
   console.log(typeof(parsedBatchesJSON));
   console.log(parsedBatchesJSON[1]);

   const stringifiedObj = `{
        "name" : "John",
        "age" : "31",
        "address" : {
            "city" : "Manila",
            "country" : "Philippines"
        }
   }`

   console.log(JSON.parse(stringifiedObj));